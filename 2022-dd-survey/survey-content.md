# Debian developers survey

The Debian contributors behind Freexian want to get broad feedback from
Debian developers on questions related to the usage of money in Debian.

The goal is to help us set guidelines on how we will use the
money that we would like to inject in the community through our [Debian
Project Funding](https://freexian-team.pages.debian.net/project-funding)
initiative. But hopefully the answers to the questions will also help
Debian, and its project leader, to identify how to make best use of
Debian's money.

We also want to identify improvement ideas that the Debian project would
like to see come through and where we could try to help through targeted
usage of funding. In short, we want to know what would be most valuable to
fund and what's acceptable to be funded.

We expect to use Limesurvey (surveys.debian.net) with a private link
sent to each Debian developers to be able to fill the survey. Collated
statistics will be shared publicly with the Debian community but
individual responses will be kept private.

## Introductory questions

What kind of contributor do you consider yourself?

* A hardcore contributor (contributes on a daily basis)
* A regular contributor (contributes on a weekly basis)
* An occasional contributor (contributes on a monthly basis or less)
* An 'emeritus' contributor (very sporadic contributions, used to contribute more)


What are you doing in Debian (please check all that apply) ?

* I'm maintaining packages
* I'm maintaining a service (typically a foo.debian.org or foo.debian.net)
* I maintain Debian-specific software
* I contribute to documentation
* I contribute to translations
* I help organize DebConf
* I contribute to mailing list discussions
* I vote on General Resolutions


Select the teams that you are part of:

* Release team
* FTPmasters (archive team)
* System Admistrators ("DSA")
* Security team
* Long Term Support team
* Quality Assurance ("QA")
* Debian Installer
* Debian CD images
* One (or more) packaging teams


What mailing lists are you following?

* debian-devel-announce
* debian-devel
* debian-project
* debian-vote
* debian-private


What other media do you use to connect with Debian acquaintances?

* IRC (`#debian-*` on OFTC)
* IRC (other networks)
* forums.debian.net
* Matrix
* Telegram
* Discord
* Signal
* ...

## Improvement ideas for Debian

* Are you aware of the "Grow Your Ideas" project in Debian?
  [https://salsa.debian.org/debian/grow-your-ideas](https://salsa.debian.org/debian/grow-your-ideas)
* Please rank the ideas listed below in order of most important to
  least important. You can put ideas that you find unacceptable below
  "further discussion".
    * PPA for Debian
    * Interactive web interface for bugs.debian.org where you can
      comment and manage bugs
    * ...
    * Further discussion

Note that most of the ideas have a corresponding issues in the "grow your
ideas" project, and please consider upvoting the ideas there that you
consider worthwhile to pursue. You do that by clicking the "thumbs up"
button at the top of each issue on salsa.debian.org.

*TODO: we must complete this list with concrete ideas collected out of our
own brainstorming and out of a discussion that we should start in
debian-project and/or debian-devel.*

## Usage of money in Debian

Debian has lots of money on its own, and it can benefit from external
funding on targeted projects as well. Please indicate whether you
agree/disagree with the following assertions:

* As a Debian contributor, I would be fine with having Debian pay other
  Debian contributors
* Debian should experiment with paying Debian contributors to implement
  long-awaited improvements
* Debian should experiment with paying Debian contributors to get rid
  of specific bottlenecks (e.g. NEW processing)


What kind of work is acceptable to be paid by Debian to Debian contributors?

* Work that is limited in scope and not recurring
* Work that is a chore that not enough persons currently want to do
* Work that no volunteer picked up in X months
* Work that is meant to support an existing team of Debian volunteers
* Any work is acceptable to be paid by Debian
* No work is acceptable to be paid by Debian


What kind of work is acceptable to be paid by external funding to Debian contributors?

* Work that is limited in scope and not recurring
* Work that is a chore that not enough persons currently want to do
* Work that no volunteer picked up in X months
* Work that is meant to support an existing team of Debian volunteers
* Any work is acceptable to be paid by external funding
* No work is acceptable to be paid by external funding


On what provisions would you be happy with Debian contributors being paid?

* I have a fair chance to apply for a similar funding
* The paid work is always reviewed/controlled by another volunteer
  Debian contributor
* The paid work is planned and implemented in the open, e.g. using
  Salsa, Debian mailing lists and/or the BTS, and is open to
  contributions from anyone in Debian.
* The paid work complies with the Debian Free Software Guidelines
  (DFSG)
* Where the paid work is packaging, the paid work results in uploads to
  Debian 'main' (i.e. no paid work should benefit to non-free packages)
* The paid work uses tools added to or already in Debian 'main'
* Debian teams may not consist solely of paid contributors
* Paid contributors do not outnumber volunteer contributors in the same
  team


For each possible use of Debian's money quoted below, please indicate if
such a use is a "good idea", "good idea in some specific cases", "bad
idea" or if it's "totally unacceptable". Select "uncertain" if you can't
make up your mind.

* Paying for package maintenance (handling bugs, new upstream release,
  improving packaging, etc.)
* Paying for the initial packaging of software new to Debian.
* Paying for development of new features/improvements for
  Debian-specific infrastructure (e.g. bugs.debian.org,
  tracker.debian.org, dak, etc.)
* Paying development of new features/improvements to Debian specific
  software (e.g. dpkg, apt, debhelper, lintian, etc.)
* Paying development of new tools to experiment new workflows or new
  services
* Paying technical writers to improve the documentation for new
  contributors
* Use Debian funds to help the Debian Project Leader (DPL) role in some
  way
* Pay Debian contributors to complete large scale changes in a
  reasonable time frame
* Pay specialist porters to support release architectures where
  donations or grants are not available. **[RH: Should it be "volunteers"
  instead of "donations or grants" ? porters are humans, donations and
  grants are not humans...]**


Assuming that Debian contributors can request "grants" to help them pursue
some well-defined project, what body should be in charge of
selecting/choosing the grants ?

* The Debian Project Leader (DPL)
* The Debian technical committee
* The Debian developers at large (e.g. through some vote)
* A new team elected for that specific purpose
* A new team designated by the DPL
* The donors who contributed that money
* None of the above

*RH: Maybe this should be a traditional condorcet vote where you rank
acceptable options above "none of the above" by order of preference*


For each role listed below, please answer the following question: Should
this Debian role include a stipend or otherwise be funded to allow more
time to fulfill the obligations of the role? (yes/no/unsure/no opinion)

* Debian Project Leader (DPL)
* Debian Release Manager
    * In general
    * During the freeze
* Member of the archive team ("ftpmasters")
    * In general
    * Those who process NEW and RM
* Member of the Security team
* Member of the LTS team
* Member of the Technical Committee


Are you concerned that a team of paid contributors could outpace the
ability of Debian volunteers to participate?

* Yes
* Somewhat
* No
* I don't know


Were you aware of the possibility to request a grant from Freexian for
projects improving Debian? It's our [Project
Funding](https://freexian-team.pages.debian.net/project-funding)
initiative.

* Yes
* No


If you have reservations or concerns about paid work that are not included
in the survey, please share them below:

[Multi-line text field]
